﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShooting : MonoBehaviour
{

    public GameObject bullet;
    public GameObject turretRotator;
    public float fireRate = 1f;

    private float cooldown = 0;

    // Start is called before the first frame update
    void Start()
    {
        transform.localPosition += transform.forward * 0.8f;
    }

    // Update is called once per frame
    void Update()
    {
        cooldown -= Time.deltaTime;

        if (Input.GetButtonDown("Fire1"))
        {
            Fire();
        }
    }

    void Fire()
    {
        if (cooldown > 0)
        {
            return;
        }
        GameObject bulletClone = Instantiate(bullet, turretRotator.transform.position, turretRotator.transform.rotation);
        cooldown = fireRate;
    }
}
