﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankControls : MonoBehaviour
{
    public Camera cam;
    public GameObject turret;
    public float maxSpeed = 10f;
    public float acceleration = 10f;
    public float brakeSpeed = 10f;
    public float torque = 10f;
    public float turretRotationSpeed = 10f;

    private Rigidbody2D rb2d;
    private bool shouldMoveForward = false;
    private bool shouldMoveBack = false;
    private bool stopTurretRotation = false;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // Change in inputmanager
        if (Input.GetKey(KeyCode.W))
        {
            shouldMoveForward = true;
        }
        else
        {
            shouldMoveForward = false;
        }

        if (Input.GetKey(KeyCode.S))
        {
            shouldMoveBack = true;
        }
        else
        {
            shouldMoveBack = false;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(Delay());
        }

        turret.transform.position = transform.position;
        RotateTurret();
    }

    private void FixedUpdate()
    {
        if (shouldMoveForward)
        {
            rb2d.AddForce(transform.up * acceleration);
        }
        if (shouldMoveBack)
        {
            rb2d.AddForce(-transform.up * brakeSpeed);
        }

        float turn = Input.GetAxis("Horizontal");
        rb2d.AddTorque(torque * -turn);
    }

    void RotateTurret()
    {
        if (stopTurretRotation)
        {
            return;
        }
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = cam.transform.position.y - turret.transform.position.y;
        Vector3 mouseWorldPos = cam.ScreenToWorldPoint(mousePos);
        float angle = Mathf.Atan2(turret.transform.position.y - mouseWorldPos.y, turret.transform.position.x - mouseWorldPos.x) * Mathf.Rad2Deg;
        float step = turretRotationSpeed * Time.deltaTime;
        turret.transform.rotation = Quaternion.RotateTowards(turret.transform.rotation, Quaternion.Euler(0, 0, angle + 90f), step);
    }

    IEnumerator Delay()
    {
        stopTurretRotation = true;
        yield return new WaitForSeconds(1f);
        stopTurretRotation = false;
    }
}
